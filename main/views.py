from django.shortcuts import render


def index(request):
    context = {}
    
    accordion = {}
    
    accordion['aktivitas'] = {
        'nama' : 'Aktivitas saat ini',
        'daftar' : ['Bermain permainan papan', 'Bermain tic-tac-toe', 'Minum coklat panas', 'Bermain tic-tac-toe', 'Menyelesaikan puzzle', 'Menonton TV', 'Minum coklat panas', 'Menyelesaikan puzzle', 'Bermain permainan papan', 'Minum teh panas']
    }
    
    accordion['organisasi'] = {
        'nama' : 'Pengalaman organisasi',
        'daftar' : ['Karang Taruna Dukuh Teguhrejo']
    }
    
    accordion['panitia'] = {
        'nama' : 'Pengalaman kepanitiaan',
        'daftar' : ['Scientific Comitee CPC Compfest 12']
    }
    
    accordion['prestasi'] = {
        'nama' : 'Prestasi',
        'daftar' : ['Peringkat 1 Lomba MIPA Kecamatan Teras', 'Peringkat 4 NPC Schematics 2018']
    }
    
    accordion['pendidikan'] = {
        'nama' : 'Pendidikan',
        'daftar' : ['BA 1 Mojolegi', 'SDN 1 Mojolegi', 'SMPN 1 Banyudono', 'SMAN 1 Boyolali']
    }
    
    context['accordion'] = accordion
    
    return render(request, 'main/index.html', context)
