$(function(){
    $("#accordion").accordion({
        collapsible: "true",
        active: "false",
        heightStyle: "content"
    }); 
    
    $(".panah").click(function(x){
        x.stopPropagation();
    });
});

function move(x, dir){
    var header = $(x).parent();
    var control = $("#" + header.attr("aria-controls"));
    
    if(dir == "up"){
        header.insertBefore(header.prev().prev());
        control.insertBefore(control.prev().prev());
    }
    else{
        control.insertAfter(control.next().next());
        header.insertAfter(header.next().next());
    }
}